BreadUp API
===================

Required software
-------------
 1. Maven
 4. MySQL

Setup development environment
-------------

 1. Create database in mysql 5.x

 2. Clone repo from BitBucket:    

Build the project
-------------

 - Build the project using maven command:  
`mvn package`

 - Build the project using maven but without tests run:  
`mvn package -Dmaven.test.skip=true`

- In windows: 
`mvn package "-Dmaven.test.skip=true"`

- Running project locally
`mvn clean package jetty:run`

Publish on heroku  
-------------
* Login with your account `heroku login`
* Create Heroku instance `heroku create`
* `git checkout -b new-branch-name`  
* Change database configuration in `src/main/resources/hibernate.cfg.xml`  
* `git add .`  
* `git commit -m "New Release <Date>"`  
* `git push heroku new-branch-name:master -f`  

----------