package cr.pernix.webapi.resources;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.jboss.logging.Logger;
import org.junit.Assert;
import org.junit.Test;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.objects.LoginObject;
import cr.pernix.webapi.services.ClientService;
import cr.pernix.webapi.services.LoginService;

public class LoginResourceTest extends JerseyTest {

    private static Logger LOG = Logger.getLogger(ClientResourceTest.class);

    private final String EMAIL = "info@pernix-solutions.com";
    private final String PASSWORD = "progra";
    private final String FULL_NAME = "N/A";
    private final String ADDRESS = "N/A";
    private final Integer PHONE = 9716371;
    private final String HOUSE_NUMBER = "N/A";
    private final String CITY = "N/A";
    

    private ClientService clientService = ClientService.getInstance();

    private Client insertTestClient() {
        Client testClient = new Client();
        testClient.setEmail(EMAIL);
        testClient.setPassword(PASSWORD);
        testClient.setAddress(ADDRESS);
        testClient.setCity(CITY);
        testClient.setFullName(FULL_NAME);
        testClient.setHouseNumber(HOUSE_NUMBER);
        clientService.save(testClient);
        return testClient;
    }

    private void deleteAll(Client client) {
        clientService.delete(client.getId());
    }

    @Override
    protected Application configure() {
        return new ResourceConfig(LoginResource.class);
    }

    @Test
    public void testLogin() {
        Client clientTmp = insertTestClient();
        LoginObject login = new LoginObject();
        LoginService loginService = LoginService.getInstance();
        login.setEmail(EMAIL);
        login.setPassword(PASSWORD);
        final String path = "login";
        final Response response = target().path(String.format(path)).request().post(Entity.json(login));
        Assert.assertEquals(200, response.getStatus());
        Client client = response.readEntity(Client.class);
        Assert.assertNotNull(client);
        clientTmp = loginService.login(login);
        deleteAll(clientTmp);
    }

}
