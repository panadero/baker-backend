package cr.pernix.webapi.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import cr.pernix.webapi.models.Bread;
import cr.pernix.webapi.services.BreadService;

public class BreadResourceTest extends JerseyTest{
	
    private final String BREAD_NAME = "Ciabbbatta";
    private final Integer STOCK = 100;
    private final Double PRICE = 800.0; 
    private Integer currentId = null;
    private BreadService breadService = new BreadService().getInstance();
	
    @Override
    protected Application configure() {
        return new ResourceConfig(BreadResource.class);
    }
    
    private List<Bread> setBreads(int amount) {
        List<Bread> testBreadList = new ArrayList<>();
        Bread testBread = new Bread();
        for (; amount > 0; amount--) {
            testBread.setBreadType(BREAD_NAME);
            testBread.setStock(STOCK);
            testBread.setPrice(PRICE);
            breadService.save(testBread);
            currentId = testBread.getId();
            testBreadList.add(testBread);
        }
        return testBreadList;
    }
    
    private boolean getInfoById(int id){
        Bread bread = breadService.get(id);
        if (bread != null){
    	    return true;
        }
    	return false;
    }
    
    private void getInfoByBreadType (String type){
        for(int id = currentId; id>0; id--){
    	    Bread bread = breadService.get(id);
        	if (bread != null && bread.getBreadType().equals(type)){
                break;
            }
    	}
    }
    
    private Bread[] getAll(int max){
    	List<Bread> breadList = new ArrayList<>(); 
    	for(; max >= 0; max--){
    		Bread bread  = breadService.get(max);
    		if(bread != null){
    			breadList.add(breadService.get(max));
    		}
    	}
    	
    	Object[] listArray = breadList.toArray();
    	Bread[] breadArray = new Bread[listArray.length];
    	short currentBread = 0; 
    	for(Object bread : listArray){
    		breadArray[currentBread++] = ((Bread) bread);
    	}
    	return breadArray; 
    }
    
    private void deleteAll(List<Bread> breadList) {
        for (Bread bread : breadList) {
            breadService.delete(bread.getId());
        }
    }
    
    @Test
    public void testGetAll() {
    	List<Bread> testBreadList = setBreads(1);
        Assert.assertTrue(testBreadList.size() > 0);
        final Response response = target().path("bread").request().get();
        Assert.assertEquals(200, response.getStatus());
        List<Bread> breadList = response.readEntity(new GenericType<List<Bread>>(){});
        Assert.assertEquals(testBreadList.size(), breadList.size());
        deleteAll(testBreadList);
    }
    
    @Test
    public void testGetById(){
        List<Bread> testBreadList = setBreads(1);
        Assert.assertTrue(testBreadList.size() > 0);
        Bread currentBread = testBreadList.get(0);
        final String path = "bread/%d";
        final Response response = target().path(String.format(path, currentBread.getId())).request().get();
        Assert.assertEquals(200, response.getStatus());
        Bread bread = response.readEntity(Bread.class);
        Assert.assertTrue("Object do not match", bread.equals(currentBread));
        deleteAll(testBreadList);
    }

    @Test
    public void testGetByBreadType(){
        List<Bread> testBreadList = setBreads(1);
        getInfoByBreadType(BREAD_NAME);
        Assert.assertTrue(testBreadList.size() > 0);
        Bread currentBread = testBreadList.get(0);
        String path = "bread/type/%s";
        final Response response = target().path(String.format(path, currentBread.getBreadType())).request().get();
        Assert.assertEquals(200, response.getStatus());
        Bread bread = response.readEntity(Bread.class);
        Assert.assertTrue("Object do not match", bread.equals(currentBread));
        deleteAll(testBreadList);
    }
    
}
