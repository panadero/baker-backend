package cr.pernix.webapi.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.models.Order;
import cr.pernix.webapi.models.Bread;
import cr.pernix.webapi.models.BreadOrder;
import cr.pernix.webapi.models.OrderType;
import cr.pernix.webapi.services.ClientService;
import cr.pernix.webapi.services.OrderService;
import cr.pernix.webapi.services.BreadOrderService;
import cr.pernix.webapi.services.BreadService;
import cr.pernix.webapi.services.OrderTypeService;

public class BreadOrderResourceTest extends JerseyTest {
    
    private final Integer AMOUNT =  7;
    private final Double PRICE = 5000.0;
    
    
    private final String FULL_NAME = "Pernix Solutions";
    private final Integer PHONE = 86136292;
    private final String ADDRESS = "N/A";
    private final String CITY = "CARTAGO";
    private final String HOUSE_NUMBER = "N/A";
    private final String EMAIL = "tesaqqatwwdbaDdadLsddfA@pernix-solutions.com";
    private final String PASSWORD = "progra";
    private final Boolean STATE = true;
    private final Date DELIVERY_DATE = new Date(2016, 9, 26);
    private final String DAY = "MONDAY";
    private final String ORDER_TYPE = "RECURRENTE"; 
    
    private final String TEST_BREAD = "BREADDwwLfadsadqdaAx";
    private final String TEST_ORDER_TYPE = "OTDdwdEsdsdwqaLsfdbb";
    
    private BreadOrderService breadOrderService = BreadOrderService.getInstance();
    private ClientService clientService = ClientService.getInstance();
    private OrderService orderService = OrderService.getInstance();
    private OrderTypeService orderTypeService = OrderTypeService.getInstance();
    private BreadService breadService = BreadService.getInstance();

    @Override
    protected Application configure() {
        return new ResourceConfig(BreadOrderResource.class);
    }
    
    private Order insertTestOrder() {
        Order testOrder = new Order();
        testOrder.setDay(DAY);
        testOrder.setDeliveryDate(DELIVERY_DATE);
        testOrder.setState(STATE);
        testOrder.setOrderType(insertTestOrderType());
        testOrder.setClient(insertTestClient());
        orderService.save(testOrder);
        return testOrder;
    }
    
    private OrderType insertTestOrderType() {        
        OrderType testOrderType = new OrderType();
        testOrderType.setOrderType(TEST_ORDER_TYPE);
        orderTypeService.save(testOrderType);
        return testOrderType;
    }
    
    private Client insertTestClient() {        
        Client testClient = new Client();
        testClient.setFullName(FULL_NAME);
        testClient.setPhone(PHONE);
        testClient.setAddress(ADDRESS);
        testClient.setCity(CITY);
        testClient.setHouseNumber(HOUSE_NUMBER);
        testClient.setEmail(EMAIL);
        testClient.setPassword(PASSWORD);
        clientService.save(testClient);
        return testClient;
    }
    
    private Bread insertTestBread() {
        Bread testBread = new Bread();
        testBread.setBreadType(TEST_BREAD);
        testBread.setStock(5);
        testBread.setPrice(1000.0);
        breadService.save(testBread);
        return testBread;
    }
    
    private List<BreadOrder> insertTestBreadOrders(int count) {
        List<BreadOrder> testBreadOrders = new ArrayList<>();
        for (; count > 0; count--) {
            BreadOrder testBreadOrder = new BreadOrder();
            testBreadOrder.setAmount(AMOUNT);
            testBreadOrder.setPrice(PRICE);
            testBreadOrder.setBread(insertTestBread());
            testBreadOrder.setOrder(insertTestOrder());
            testBreadOrder.setState(false);
            breadOrderService.save(testBreadOrder);
            testBreadOrders.add(testBreadOrder);
        }
        return testBreadOrders;
    }
    
    private void deleteAll(List<BreadOrder> breadOrderList) {
        for (BreadOrder breadOrder : breadOrderList) {
            breadOrderService.delete(breadOrder.getId());
        }
    }
    
    @Test
    public void testGet() {
        List<BreadOrder> testBreadOrderTest = insertTestBreadOrders(1);
        Assert.assertTrue(testBreadOrderTest.size() > 0);
        final Response response = target().path("breadorder").request().get();
        Assert.assertEquals(200, response.getStatus());
        List<BreadOrder> breadOrderList = response.readEntity(new GenericType<List<BreadOrder>>() {});
        Assert.assertEquals(testBreadOrderTest.size(), breadOrderList.size());
        deleteAll(breadOrderList);
    }
}
