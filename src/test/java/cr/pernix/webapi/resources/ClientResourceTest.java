package cr.pernix.webapi.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.services.ClientService;

public class ClientResourceTest extends JerseyTest {
	
    private final String FULL_NAME = "Pernix Solutions";
    private final Integer PHONE = 86136292;
    private final String ADDRESS = "N/A";
    private final String CITY = "CARTAGO";
    private final String HOUSE_NUMBER = "N/A";
    private final String EMAIL = "hmuir@pernix-solutions.com";
    private final String PASSWORD = "progra";
    
    private ClientService clientService = ClientService.getInstance();
    
    private List<Client> insertTestClient(int count) {
        List<Client> testClientList = new ArrayList<>();
        for (; count > 0; count--) {
            Client testClient = new Client();
            testClient.setFullName(FULL_NAME);
            testClient.setPhone(PHONE);
            testClient.setAddress(ADDRESS);
            testClient.setCity(CITY);
            testClient.setHouseNumber(HOUSE_NUMBER);
            testClient.setEmail(EMAIL);
            testClient.setPassword(PASSWORD);
            clientService.save(testClient);
            testClientList.add(testClient);
        }
        return testClientList;
    }
    
    private void deleteAll(List<Client> clientList) {
        for (Client client : clientList) {
        	clientService.delete(client.getId());
        }
    }
    
    @Override
    protected Application configure() {
        return new ResourceConfig(ClientResource.class);
    }
    
    @Test
    public void testGet() {
        List<Client> testClientTest = insertTestClient(1);
        Assert.assertTrue(testClientTest.size() > 0);
        final Response response = target().path("client").request().get();
        Assert.assertEquals(200, response.getStatus());
        List<Client> clientList = response.readEntity(new GenericType<List<Client>>() {});
        Assert.assertEquals(testClientTest.size(), clientList.size());
        deleteAll(clientList);
    }
    
    @Test
    public void testGetClient() {
        List<Client> testClientTest = insertTestClient(1);
        Assert.assertTrue(testClientTest.size() > 0);
        Client clientCompare = testClientTest.get(0);
        final String path = "client/%d";
        final Response response = target().path(String.format(path, clientCompare.getId())).request().get();
        Assert.assertEquals(200, response.getStatus());
        Client client = response.readEntity(Client.class);
        Assert.assertTrue("Object do not match", client.equals(clientCompare));
        deleteAll(testClientTest);
    }

    @Test
    public void testEditClient() {
        List<Client> testClient = insertTestClient(1);
        Assert.assertTrue(testClient.size() > 0);
        Client updateClient = testClient.get(0);
        updateClient.setFullName("Prueba Test");
        final Response response = target().path("client").request().put(Entity.json(updateClient), Response.class);
        Assert.assertEquals(200, response.getStatus());
        Client clientUpdated = ClientService.getInstance().get(updateClient.getId());
        Assert.assertTrue("Not the same object", clientUpdated.equals(updateClient));
        Assert.assertNotEquals("Name not modified", FULL_NAME, clientUpdated.getFullName());
        clientService.delete(updateClient.getId());
        deleteAll(testClient);
    }
   
    @Test
    public void testPostClient() {
        Client testClient = new Client();
        testClient.setFullName(FULL_NAME);
        testClient.setPhone(PHONE);
        testClient.setAddress(ADDRESS);
        testClient.setCity(CITY);
        testClient.setHouseNumber(HOUSE_NUMBER);
        testClient.setEmail(EMAIL);
        testClient.setPassword(PASSWORD);
        final Response response = target().path("client").request().post(Entity.json(testClient), Response.class);
        Assert.assertEquals(200, response.getStatus());
        Client clientSaved = response.readEntity(Client.class);
        List<Client> clientList = new ArrayList<>();
        clientList.add(clientSaved);
        deleteAll(clientList);
    }
}
