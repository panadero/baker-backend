package cr.pernix.webapi.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.models.Order;
import cr.pernix.webapi.models.OrderType;
import cr.pernix.webapi.services.ClientService;
import cr.pernix.webapi.services.OrderService;
import cr.pernix.webapi.services.OrderTypeService;

public class OrderResourceTest extends JerseyTest {
			
    private final String FULL_NAME = "Pernix Solutions";
    private final Integer PHONE = 86136292;
    private final String ADDRESS = "N/A";
    private final String CITY = "CARTAGO";
    private final String HOUSE_NUMBER = "N/A";
    private final String EMAIL = "hmuir@pernix-solutions.com";
    private final String PASSWORD = "progra";
    private final Boolean STATE = true;
    private final Date DELIVERY_DATE = new Date(2016, 9, 26);
    private final String DAY = "MONDAY";
    private final String ORDER_TYPE = "RECURRENTE"; 
	
    private OrderService orderService = OrderService.getInstance();
    private OrderTypeService orderTypeService = OrderTypeService.getInstance();
    private ClientService clientService = ClientService.getInstance();
    
    
    @Override
    protected Application configure() {
        return new ResourceConfig(OrderResource.class);
    }
    
    private OrderType insertTestOrderType() {        
        OrderType testOrderType = new OrderType();
        testOrderType.setOrderType(ORDER_TYPE);
        orderTypeService.save(testOrderType);
        return testOrderType;
    }
    
    private Client insertTestClient(int count) {        
        Client testClient = new Client();
        testClient.setFullName(FULL_NAME);
        testClient.setPhone(PHONE);
        testClient.setAddress(ADDRESS);
        testClient.setCity(CITY);
        testClient.setHouseNumber(HOUSE_NUMBER);
        testClient.setEmail(EMAIL + count);
        testClient.setPassword(PASSWORD);
        clientService.save(testClient);
        return testClient;
    }
    
    private List<Order> insertTestOrders(int count) {
        List<Order> testOrders = new ArrayList<>();
        for (; count > 0; count--) {
            Order testOrder = new Order();
            testOrder.setDay(DAY);
            testOrder.setDeliveryDate(DELIVERY_DATE);
            testOrder.setState(STATE);
            testOrder.setOrderType(insertTestOrderType());
            testOrder.setClient(insertTestClient(count));
            orderService.save(testOrder);
            testOrders.add(testOrder);
        }
        return testOrders;
    }
    
    private void deleteAll(List<Order> orderList) {
        for (Order order : orderList) {
        	orderService.delete(order.getId());
        	orderTypeService.delete(order.getOrderType().getId());
        	clientService.delete(order.getClient().getId());
        }
    }
    
    @Test
    public void testGet() {
        List<Order> testOrderTest = insertTestOrders(1);
        Assert.assertTrue(testOrderTest.size() > 0);
        final Response response = target().path("order").request().get();
        Assert.assertEquals(200, response.getStatus());
        List<Order> orderList = response.readEntity(new GenericType<List<Order>>() {});
        Assert.assertEquals(testOrderTest.size(), orderList.size());
        deleteAll(orderList);
    }
	
    @Test
    public void testGetOrder() {
    	List<Order> testOrderTest = insertTestOrders(1);
        Assert.assertTrue(testOrderTest.size() > 0);
        Order orderCompare = testOrderTest.get(0);
        final String path = "order/%d";
        final Response response = target().path(String.format(path, orderCompare.getId())).request().get();
        Assert.assertEquals(200, response.getStatus());
        Order order = response.readEntity(Order.class);
        Assert.assertTrue("Object do not match", order.equals(orderCompare));
        deleteAll(testOrderTest);
    }
    
    @Test
    public void testPostOrder() {
        Order testOrder = new Order();
        testOrder.setDay(DAY);
        testOrder.setDeliveryDate(DELIVERY_DATE);
        testOrder.setState(STATE);
        testOrder.setOrderType(insertTestOrderType()); 
        testOrder.setClient(insertTestClient(1));
        final Response response = target().path("order").request().post(Entity.json(testOrder), Response.class);
        Assert.assertEquals(200, response.getStatus());
        Order orderSaved = response.readEntity(Order.class);
        List<Order> orderList = new ArrayList<>();
        orderList.add(orderSaved);
        deleteAll(orderList);
    }
    
    @Test
    public void testChangeState() {
        List<Order> testOrder = insertTestOrders(1);
        Assert.assertTrue(testOrder.size() > 0);
        Order orderChangeState = testOrder.get(0);
        Response response = target().path("order/changeState").request().put(Entity.json(orderChangeState), Response.class);
        Assert.assertEquals(200, response.getStatus());
        Order order = orderService.get(orderChangeState.getId());
        Assert.assertFalse(order.getState());
        response = target().path("order/changeState").request().put(Entity.json(order), Response.class);
        Assert.assertEquals(200, response.getStatus());
        order = orderService.get(order.getId());
        Assert.assertTrue(order.getState());
        deleteAll(testOrder);
    }
    
}
