package cr.pernix.webapi.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ordersready", schema = "breadup")
@XmlRootElement
public class OrderReady implements java.io.Serializable {

    private static final long serialVersionUID = -5523730234720184370L;
    private int id;
    private Date currentDate;
    private Order order;
	
    public OrderReady() {
    }
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
    	return id;
    }
	
    public void setId(int id) {
    	this.id = id;
    }
	
    @Column(name = "order_date")
    public Date getCurrentDate() {
    	return currentDate;
    }
	
    public void setCurrentDate(Date currentDate) {
    	this.currentDate = currentDate;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_orderId", nullable = false)		
    public Order getOrder() {
    	return order;
    }

    public void setOrder(Order order) {
    	this.order = order;
    }
	
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        OrderReady order = (OrderReady) o;
        if (id != order.id)
            return false;
        return true;
    }
	
}
