package cr.pernix.webapi.models;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ordertypes", schema = "breadup")
@XmlRootElement
public class OrderType implements java.io.Serializable {

	private static final long serialVersionUID = 9134092432504922910L;
	private int id;
	private String orderType;
    private Set<Order> order = new HashSet<Order>(0);
	
	public OrderType() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "order_type", unique = true, nullable = false)
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}	
	
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ordertypes")
    public Set<Order> order() {
        return this.order;
    }

    public void setOrder(Set<Order> order) {
        this.order = order;
    }
	
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        OrderType orderType = (OrderType) o;
        if (id != orderType.id)
            return false;
        return true;
    }
}
