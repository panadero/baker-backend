package cr.pernix.webapi.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "orders", schema = "breadup")
@XmlRootElement
public class Order implements java.io.Serializable {

    private static final long serialVersionUID = 2487918197965735796L;
    private int id;
    private Date deliveryDate;
    private Boolean orderState;
    private String day;
    private Client client;
    private OrderType orderType;
    private String pushToken;
    private String cardToken;
    private String chargeToken;
    private Set<BreadOrder> breadOrder = new HashSet<BreadOrder>(0);
    private Set<OrderReady> orderReady = new HashSet<OrderReady>(0);

    public Order() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "delivery_date")
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date orderDate) {
        this.deliveryDate = orderDate;
    }

    @Column(name = "order_state")
    public Boolean getState() {
        return orderState;
    }

    public void setState(Boolean state) {
        this.orderState = state;
    }

    @Column(name = "day")
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_clientId", nullable = false)
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_orderTypeId", nullable = false)
    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    @Column(name = "push_token")
    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    @Column(name = "card_token")
    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    @Column(name = "charge_token")
    public String getChargeToken() {
        return chargeToken;
    }

    public void setChargeToken(String chargeToken) {
        this.chargeToken = chargeToken;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "breadorders")
    public Set<BreadOrder> breadOrder() {
        return this.breadOrder;
    }

    public void setBreadOrder(Set<BreadOrder> breadOrder) {
        this.breadOrder = breadOrder;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ordersready")
    public Set<OrderReady> orderReady() {
        return orderReady;
    }

    public void setOrderReady(Set<OrderReady> orderReady) {
        this.orderReady = orderReady;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Order order = (Order) o;
        if (id != order.id)
            return false;
        return true;
    }
}
