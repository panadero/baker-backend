package cr.pernix.webapi.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "breads", schema = "breadup")
@XmlRootElement
public class Bread implements java.io.Serializable{

    private static final long serialVersionUID = 8706381562994006316L;
    private int id;
    private String breadType;
    private int stock;
    private Double price;
    private Set<BreadOrder> breadOrder = new HashSet<BreadOrder>(0);

    public Bread() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "bread_type", unique = true, nullable = false)
    public String getBreadType() {
        return breadType;
    }

    public void setBreadType(String breadType) {
        this.breadType = breadType;
    }

    @Column(name = "stock")
    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Column(name = "price")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "breads")
    public Set<BreadOrder> breadOrder() {
        return this.breadOrder;
    }

    public void setBreadOrder(Set<BreadOrder> breadOrder) {
        this.breadOrder = breadOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Bread bread = (Bread) o;
        if (id != bread.id)
            return false;
        return true;
    }
}
