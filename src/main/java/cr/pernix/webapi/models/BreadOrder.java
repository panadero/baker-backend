package cr.pernix.webapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "breadorders", schema = "breadup")
@XmlRootElement
public class BreadOrder implements java.io.Serializable {

    private static final long serialVersionUID = -666524003438581436L;
    private int id;
    private int amount;
    private Double price;
    private boolean state;
    private Bread bread;
    private Order order;
	
    public BreadOrder() {
    }
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    @Column(name = "amount")
    public int getAmount() {
    	return amount;
    }
    
    public void setAmount(int amount) {
    	this.amount = amount;
    }

    @Column(name = "price")
    public Double getPrice() {
    	return price;
    }
    
    public void setPrice(Double price) {
    	this.price = price;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_breadTypeId", nullable = false)
    public Bread getBread() {
    	return bread;
    }
    
    public void setBread(Bread bread) {
    	this.bread = bread;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_orderId", nullable = false)
    public Order getOrder() {
    	return order;
    }

    public void setOrder(Order order) {
    	this.order = order;
    }

    @Column(name = "state")
    public boolean isState() {
    	return state;
    }

    public void setState(boolean state) {
    	this.state = state;
    }

	@Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BreadOrder breadOrder = (BreadOrder) o;
        if (id != breadOrder.id)
            return false;
        return true;
    }
 
}
