package cr.pernix.webapi.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.utils.HibernateUtil;

public class ClientService {

	private static volatile ClientService instance = null;
	private static final Log LOGGER = LogFactory.getLog(ClientService.class);
	
	public ClientService() {
	}
	
	public static synchronized ClientService getInstance() {
		if(instance == null) {
			instance = new ClientService();
		}
		return instance;
	}
	
	public List<Client> get() {
		return get(0, 0);
	}
	
	public List<Client> get(int firstResult, int maxResult) {
        List<Client> list = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from Client");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List<Client> allClients = query.list();
        transaction.commit();
        for (Object clientObject : allClients) {
            Client client = (Client) clientObject;
            list.add(client);
        }
        return list;
	}
	
    public Client get(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Client client = (Client) session.get(Client.class, id);
        transaction.commit();
        return client;
    }
	
    public void save(Client client) {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(client);
        transaction.commit();
    }

    public void delete(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Client client = get(id);
        if (client != null) {
            if (!session.isOpen()) {
                LOGGER.debug(" >>> Session close.");
                LOGGER.debug(" >>> Reopening session.");
                session = HibernateUtil.getSessionFactory().openSession();
            }
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            session.delete(client);
            transaction.commit();
        }
    }
}
