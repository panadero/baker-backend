package cr.pernix.webapi.services;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.objects.LoginObject;
import cr.pernix.webapi.objects.LoginObject;
import cr.pernix.webapi.utils.HibernateUtil;

public class LoginService {
    private static volatile LoginService instance = null;
    private static final Log LOGGER = LogFactory.getLog(ClientService.class);

    private LoginService() {
    }

    public static synchronized LoginService getInstance() {
        if (instance == null) {
            instance = new LoginService();
        }
        return instance;
    }
    
    public boolean authenticate(String authCredentials) {
        if (null == authCredentials)
            return false;

        final String encodedClientPassword = authCredentials.replaceFirst("Basic"
                + " ", "");
        String emailAndPassword = null;
        try {
            byte[] decodedBytes = BaseEncoding.base64().decode(encodedClientPassword);
            emailAndPassword = new String(decodedBytes, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        final StringTokenizer tokenizer = new StringTokenizer(
        		emailAndPassword, ":");
        final String email = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
        
        // TODO: Provisional Admin authentication
        if (email.contentEquals("allowAdmin")) {
        		return true;
        } else {
        		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            Query query = session.createQuery("from Client c where c.email = :email");
            query.setParameter("email", email);
            @SuppressWarnings("unchecked")
            List<Client> allClients = query.list();
            transaction.commit();
            if(allClients.size() > 0) {
                if(allClients.get(0).getPassword().contentEquals(password)) {
                    return true;
                }
            }
            return false;
        }
    }

    public Client login(LoginObject loginObject) {
    	Client client = new Client();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from Client c where c.email = :email");
        query.setParameter("email", loginObject.getEmail());
        @SuppressWarnings("unchecked")
        List<Client> allClients = query.list();
        transaction.commit();
        if (allClients.size() > 0) {
        	client = (Client)allClients.get(0);
        }
        else{  
            Client clientTmp = new Client();
            clientTmp.setEmail(loginObject.getEmail());
            clientTmp.setPassword(Hashing.sha1().hashString(loginObject.getPassword(), Charsets.UTF_8 ).toString());
            ClientService.getInstance().save(clientTmp);
            client = clientTmp;
        }
        return client;
    }
}
