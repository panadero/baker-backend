package cr.pernix.webapi.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import cr.pernix.webapi.models.OrderType;
import cr.pernix.webapi.utils.HibernateUtil;

public class OrderTypeService {

	private static volatile OrderTypeService instance = null;
	private static final Log LOGGER = LogFactory.getLog(OrderTypeService.class);
	
	public OrderTypeService() {
	}
	
	public static synchronized OrderTypeService getInstance() {
		if(instance == null) {
			instance = new OrderTypeService();
		}
		return instance;
	}
	
	public List<OrderType> get() {
		return get(0, 0);
	}
	
	public List<OrderType> get(int firstResult, int maxResult) {
        List<OrderType> list = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from Answer");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List<OrderType> allOrderTypes = query.list();
        transaction.commit();
        for (Object orderTypeObject : allOrderTypes) {
        	OrderType orderType = (OrderType) orderTypeObject;
            list.add(orderType);
        }
        return list;
	}
	
    public OrderType get(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        OrderType orderType = (OrderType) session.get(OrderType.class, id);
        transaction.commit();
        return orderType;
    }
	
    public void save(OrderType orderType) {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(orderType);
        transaction.commit();
    }

    public void delete(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        OrderType orderType = get(id);
        if (orderType != null) {
            if (!session.isOpen()) {
                LOGGER.debug(" >>> Session close.");
                LOGGER.debug(" >>> Reopening session.");
                session = HibernateUtil.getSessionFactory().openSession();
            }
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            session.delete(orderType);
            transaction.commit();
        }
    }
}
