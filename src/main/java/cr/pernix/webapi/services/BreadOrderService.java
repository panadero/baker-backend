package cr.pernix.webapi.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import cr.pernix.webapi.models.BreadOrder;
import cr.pernix.webapi.utils.HibernateUtil;

public class BreadOrderService {
    private static volatile BreadOrderService instance = null;
    private static final Log LOGGER = LogFactory.getLog(BreadOrderService.class);

    public BreadOrderService() {
    }

    public static synchronized BreadOrderService getInstance() {
        if(instance == null) {
            instance = new BreadOrderService();
        }
        
        return instance;
    }

    public List<BreadOrder> get() {
        return get(0, 0);
    }

    public List<BreadOrder> get(int firstResult, int maxResult) {
        
        List<BreadOrder> list = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from BreadOrder");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List<BreadOrder> allBreadOrders = query.list();
        transaction.commit();
        for (BreadOrder breadOrderObject: allBreadOrders) {
            BreadOrder bread = breadOrderObject;
            list.add(bread);
        }
        
        return list;
    }
    
    public List<BreadOrder> get(Date startDate, Date endDate) {
    		List<BreadOrder> list = new ArrayList<>();
    		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        SimpleDateFormat sf=new SimpleDateFormat("YYYY-MM-dd");
        String fromDate = sf.format(startDate);
        String toDate = sf.format(endDate);
        String hql = "from BreadOrder where order.deliveryDate between '" + fromDate + "' AND '" + toDate + "' ORDER BY order.deliveryDate ASC";
        Query query = session.createQuery(hql);
        @SuppressWarnings("unchecked")
        List<BreadOrder> allBreadOrders = query.list();
        transaction.commit();
        for (BreadOrder breadOrderObject: allBreadOrders) {
            BreadOrder bread = breadOrderObject;
            list.add(bread);
        }
    		return list;
    }

    public BreadOrder get(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        BreadOrder bread = (BreadOrder) session.get(BreadOrder.class, id);
        transaction.commit();
        return bread;
    }

    public void save(BreadOrder bread) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(bread);
        transaction.commit();
    }

    public void delete(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        BreadOrder bread = get(id);
        if (bread != null) {
            if (!session.isOpen()) {
                LOGGER.debug(" >>> Session close.");
                LOGGER.debug(" >>> Reopening session.");
                session = HibernateUtil.getSessionFactory().openSession();
            }
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            session.delete(bread);
            transaction.commit();
        }
    }
}
