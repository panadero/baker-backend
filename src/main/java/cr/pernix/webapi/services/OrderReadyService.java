package cr.pernix.webapi.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import cr.pernix.webapi.models.OrderReady;
import cr.pernix.webapi.utils.HibernateUtil;

public class OrderReadyService {

    private static volatile OrderReadyService instance = null;
    private static final Log LOGGER = LogFactory.getLog(OrderService.class);
	
    public OrderReadyService() {
    }
	
    public static synchronized OrderReadyService getInstance() {
    	if(instance == null) {
    		instance = new OrderReadyService();
    	}
    	return instance;
    }
	
    public List<OrderReady> get() {
    	return get(0, 0);
    }
	
    public List<OrderReady> get(int firstResult, int maxResult) {
        List<OrderReady> list = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from OrderReady");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List<OrderReady> allOrders = query.list();
        transaction.commit();
        for (Object orderObject : allOrders) {
            OrderReady order = (OrderReady) orderObject;
            list.add(order);
        }
        return list;
    }
	
    public OrderReady get(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        OrderReady order = (OrderReady) session.get(OrderReady.class, id);
        transaction.commit();
        return order;
    }

    public void save(OrderReady order) {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(order);
        transaction.commit();
    }

    public void delete(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        OrderReady order = get(id);
        if (order != null) {
            if (!session.isOpen()) {
    
                LOGGER.debug(" >>> Session close.");
                LOGGER.debug(" >>> Reopening session.");
                session = HibernateUtil.getSessionFactory().openSession();
            }
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            session.delete(order);
            transaction.commit();
        }
    }
	
}
