package cr.pernix.webapi.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import cr.pernix.webapi.models.BreadOrder;
import cr.pernix.webapi.models.Order;
import cr.pernix.webapi.objects.DeliveryObject;
import cr.pernix.webapi.utils.HibernateUtil;

public class OrderService {

	private static volatile OrderService instance = null;
	private static final Log LOGGER = LogFactory.getLog(OrderService.class);
	
	public OrderService() {
	}
	
	public static synchronized OrderService getInstance() {
		if(instance == null) {
			instance = new OrderService();
		}
		return instance;
	}
	
	public List<Order> get() {
		return get(0, 0);
	}
	
	public List<Order> get(int firstResult, int maxResult) {
        List<Order> list = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
            LOGGER.debug(" >>> Transaction close.");
        Query query = session.createQuery("from Order");
        query.setFirstResult(firstResult);
        query.setMaxResults(maxResult);
        @SuppressWarnings("unchecked")
        List<Order> allOrders = query.list();
        transaction.commit();
        for (Object orderObject : allOrders) {
        	Order order = (Order) orderObject;
            list.add(order);
        }
        return list;
	}
	
    public Order get(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Order order = (Order) session.get(Order.class, id);
        transaction.commit();
        return order;
    }
	
    public void changeState(Order order) {
        order.setState(!order.getState());
        save(order);
    }
    
    public void save(Order order) {
    		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(order);
        transaction.commit();
    }

    public void delete(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Order order = get(id);
        if (order != null) {
            if (!session.isOpen()) {
                LOGGER.debug(" >>> Session close.");
                LOGGER.debug(" >>> Reopening session.");
                session = HibernateUtil.getSessionFactory().openSession();
            }
            Transaction transaction = session.getTransaction();
            transaction.begin();
            if (transaction.getStatus().equals(TransactionStatus.NOT_ACTIVE))
                LOGGER.debug(" >>> Transaction close.");
            session.delete(order);
            transaction.commit();
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<DeliveryObject> getDeliveries(Date startDate, Date endDate) {
    		List<DeliveryObject> list = new ArrayList<>();
    		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    		Transaction transaction = session.getTransaction();
        transaction.begin();
        SimpleDateFormat sf = new SimpleDateFormat("YYYY-MM-dd");
        String fromDate = sf.format(startDate);
        String toDate = sf.format(endDate);
        String sqlQuery = "SELECT DATE(delivery_date) as deliveryDate, c.full_name as fullName FROM ";
        sqlQuery += "orders o INNER JOIN clients c on o.fk_clientId = c.id WHERE DATE(delivery_date) ";
        sqlQuery += "between '" + fromDate + "' and '" + toDate + "' and o.fk_orderTypeId != 3 ";
        sqlQuery += "and order_state = 1 group BY deliveryDate, fullName ORDER BY deliveryDate;";
        Query query = session.createSQLQuery(sqlQuery);
        Iterator<Object> iterator = query.list().iterator();
        transaction.commit();
        
        while(iterator.hasNext()) {
        		Object[] tuple = (Object[]) iterator.next();
        		list.add(new DeliveryObject(tuple[0].toString(), tuple[1].toString()));
        }
        
    		return list;
    }
}
