package cr.pernix.webapi.objects;

public class DeliveryObject {
	
	private String deliveryDate;
	private String fullName;
	
	public DeliveryObject(String deliveryDate, String fullName) {
		this.deliveryDate = deliveryDate;
		this.fullName = fullName;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}
