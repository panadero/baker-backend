package cr.pernix.webapi.objects;

public class OrderObject {

    private int orderId;
    private String houseNumber;
	
    public int getOrderId() {
    	return orderId;
    }
	
    public void setOrderId(int orderId) {
    	this.orderId = orderId;
    }
	
    public String getHouseNumber() {
    	return houseNumber;
    }
	
    public void setHouseNumber(String houseNumber) {
    	this.houseNumber = houseNumber;
    }
	
}
