package cr.pernix.webapi.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cr.pernix.webapi.models.Bread;
import cr.pernix.webapi.services.BreadService;


@Path("bread")
public class BreadResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() throws Exception {
        List<Bread> result = BreadService.getInstance().get();
        GenericEntity<List<Bread>> list = new GenericEntity<List<Bread>>(result) {
        };
        return Response.ok(list).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id) throws Exception {
        Bread bread = BreadService.getInstance().get(id);
        return Response.ok(bread).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @GET
    @Path("/type/{name}")
    public Response get(@PathParam("name") String breadType) {
    	Bread bread = BreadService.getInstance().getBreadByType(breadType);
    	return Response.ok(bread).header("Access-Control-Allow-Origin", "*").build();
    }

}
