package cr.pernix.webapi.resources;

import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import cr.pernix.webapi.models.Order;
import cr.pernix.webapi.objects.DeliveryObject;
import cr.pernix.webapi.services.OrderService;

@Path("order")
public class OrderResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() throws Exception {
		List<Order> result = OrderService.getInstance().get();
		GenericEntity<List<Order>> list = new GenericEntity<List<Order>>(result) {
		};
		return Response.ok(list).header("Access-Control-Allow-Origin", "*").build();
	}
	
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id) throws Exception {
        Order order = OrderService.getInstance().get(id);
        return Response.ok(order).header("Access-Control-Allow-Origin", "*").build();
    }
	
    @POST
    public Response create(Order order) throws Exception {
    		OrderService.getInstance().save(order);
    		return Response.ok(order).header("Access-Control-Allow-Origin", "*").build();
    }    
    
    @GET
    @Path("/deliveries/{startDate}/{endDate}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDeliveries(
    		@PathParam("startDate") long startDateMilliseconds,
    		@PathParam("endDate") long endDateMilliseconds) throws Exception {
    	
    		Date startDate = new Date(startDateMilliseconds);
		Date endDate = new Date(endDateMilliseconds);
		List<DeliveryObject> result = OrderService.getInstance().getDeliveries(startDate, endDate);
		Gson gson = new Gson();
		return Response.ok(gson.toJson(result)).build();
	}
    
   @PUT
   @Path("/changeState")
   public Response update(Order order) throws Exception {
	   OrderService.getInstance().changeState(order);
	   return Response.ok(order).header("Access-Control-Allow-Origin", "*").build();
   }
}
