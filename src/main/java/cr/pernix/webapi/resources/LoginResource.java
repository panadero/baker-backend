package cr.pernix.webapi.resources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.objects.LoginObject;
import cr.pernix.webapi.services.LoginService;

@Path("login")
public class LoginResource {
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClient(LoginObject loginObject) throws Exception {
        Client client = LoginService.getInstance().login(loginObject);
        return Response.ok(client).header("Access-Control-Allow-Origin", "*").build();
    }
}