package cr.pernix.webapi.resources;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cr.pernix.webapi.models.Client;
import cr.pernix.webapi.services.ClientService;


@Path("client")
public class ClientResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() throws Exception {
		List<Client> result = ClientService.getInstance().get();
		GenericEntity<List<Client>> list = new GenericEntity<List<Client>>(result) {
		};
		return Response.ok(list).header("Access-Control-Allow-Origin", "*").build();
	}
	
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id) throws Exception {
        Client client = ClientService.getInstance().get(id);
        return Response.ok(client).header("Access-Control-Allow-Origin", "*").build();
    }
	
    @POST
    public Response create(Client client) throws Exception {
        ClientService.getInstance().save(client);
    	return Response.ok(client).header("Access-Control-Allow-Origin", "*").build();
    }

    @PUT
    public Response update(Client client) throws Exception {
        ClientService.getInstance().save(client);
        return Response.ok(client).header("Access-Control-Allow-Origin", "*").build();
    }
}
