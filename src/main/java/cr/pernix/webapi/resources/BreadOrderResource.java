package cr.pernix.webapi.resources;

import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cr.pernix.webapi.models.BreadOrder;
import cr.pernix.webapi.services.BreadOrderService;

@Path("breadorder")
public class BreadOrderResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() throws Exception {
        List<BreadOrder> result = BreadOrderService.getInstance().get();
        GenericEntity<List<BreadOrder>> list = new GenericEntity<List<BreadOrder>>(result) {
        };
        return Response.ok(list).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @GET
    @Path("/{startDate}/{endDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(
    		@PathParam("startDate") long startDateMilliseconds,
    		@PathParam("endDate") long endDateMilliseconds) throws Exception {
    	
    		Date startDate = new Date(startDateMilliseconds);
    		Date endDate = new Date(endDateMilliseconds);
        List<BreadOrder> result = BreadOrderService.getInstance().get(startDate, endDate);
        GenericEntity<List<BreadOrder>> list = new GenericEntity<List<BreadOrder>>(result) {
        };
        return Response.ok(list).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @POST
    public Response create(BreadOrder order) throws Exception {
        BreadOrderService.getInstance().save(order);
        return Response.ok(order).header("Access-Control-Allow-Origin", "*").build();
    }  
    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") int id) throws Exception {
        BreadOrder breadOrder = BreadOrderService.getInstance().get(id);
        return Response.ok(breadOrder).header("Access-Control-Allow-Origin", "*").build();
    }
//    
//    @GET
//    @Path("/type/{name}")
//    public Response get(@PathParam("name") String breadType) {
//        Bread bread = BreadService.getInstance().getBreadByType(breadType);
//        return Response.ok(bread).header("Access-Control-Allow-Origin", "*").build();
//    }
}
